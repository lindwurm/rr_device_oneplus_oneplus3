# Device configuration for OnePlus 3

The OnePlus 3 ( codenamed `oneplus3` ) is a high-end smartphone from OnePlus.

Release date was June 2016.

## Device specifications

| Feature | Spec |
| ------: | :--- |
| SoC | Qualcomm Snapdragon 820 `MSM8996` |
| CPU | Dual-core 2.2 GHz Kryo & dual-core 1.6 GHz Kryo |
| GPU | Adreno 530 |
| RAM | 6GB LPDDR4 |
| OS | Android 6.0 with OxygenOS 3.2 |
| Storage | 64 GB (UFS 2.0) |
| Battery | 3000 mAh (Non-removable)|
| Dimensions | 152.7 x 74.7 x 7.4 mm |
| Display | 1920 x 1080 px, 5.5" Optic AMOLED (401 ppi) |
| Rear camera  | 16 MP, flash:LED |
| Front camera | 8 MP |

## Device picture

![OnePlus3](http://wiki.cyanogenmod.org/images/6/67/Oneplus3.png)

## about `kazari-v1` branch

made for kazari-op3-kernel.